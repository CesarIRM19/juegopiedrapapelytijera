Juego Piedra, Papel y Tijera

•	El juego consiste en la selección de la piedra, papel y tijera, 
•	Tiene una cierta cantidad de créditos que el sistema pone
•	Cada valor de victoria y perdida es equivalente a los valores de 10 dividido en 3 objetos que serán piedra, papel y tijera (Este valor  no puede sumar más de 10)
•	Se puede editar estos valores, pero se bloqueará el botón de jugar, se bloqueará el botón de editar y se habilitan los campos de los valores de ganancia o perdida

