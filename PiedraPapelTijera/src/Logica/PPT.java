package Logica;

import javax.swing.JOptionPane;

public class PPT {
	public int piedra (int b,String j,String p)
    {
        int x = 0;
        
        if(b == 1)
        {
            x = 1;
            /*System.out.println("Ganaste");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"JAJAJAJA PERDISTE :(");
        }
        else if(b == 2)
        {
            x = 0;
            /*System.out.println("Perdiste");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"WOW, GANASTE :)");
            
        }
        else
        {
        	x = 2;
            /*System.out.println("Empate");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"OPS, EMPATASTE :|");
        }
        return x;
    }
    
    public int papel (int b,String j,String p)
    {
        int x = 2;
        
        if(b == 0)
        {
            x = 1;
            /*System.out.println("Ganaste");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"WOW, GANASTE :)");
        }
        else if(b == 2)
        {
            x = 0;
            /*System.out.println("Perdiste");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"JAJAJAJA PERDISTE :(");
        }
        else
        {
            /*System.out.println("Empate");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"OPS, EMPATASTE :|");
        }
        return x;
    }
    
    public void tijera (int b,String j,String p)
    {
        int x = 2;
        
        if(b == 0)
        {
            x = 0;
            /*System.out.println("Perdiste");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"JAJAJAJA PERDISTE :(");
            
        }
        else if(b == 1)
        {
            x = 1;
            /*System.out.println("Ganaste");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"WOW, GANASTE :)");
        }
        else
        {
            /*System.out.println("Empate");*/
            JOptionPane.showMessageDialog(null,j+" vs "+p+"\n"+"OPS, EMPATASTE :|");
        }
    }
    public String cadena(int x)
    {
        String m;
        if(x == 0)
        {
            m = "Piedra";
        }
        else if(x == 1)
        {
            m = "Papel";
        }
        else
        {
            m = "Tijera";
        }
        return m;
    }
    
    public int maquina()
    {
        int ran;
        ran = (int) (Math.random()*3);
        return ran;
    }
}
