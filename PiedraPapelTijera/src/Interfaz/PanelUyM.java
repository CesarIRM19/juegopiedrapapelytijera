package Interfaz;

import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class PanelUyM extends JPanel {
	JComboBox Combobox;
	
	public PanelUyM() {
		String[] juego = {"Piedra","Papel","Tijera"};
		Combobox = new JComboBox(juego);
		this.setLayout(new FlowLayout());
		this.add(Combobox);
		Combobox.setEnabled(true); 
	}
	public void bloquear() {
		Combobox.setEnabled(false); 
	}
	public void desbloquear() {
		Combobox.setEnabled(true); 
	}
	public String obtener() {
		String n;
		n = (String)Combobox.getSelectedItem();
		return n;
	}
	
}
