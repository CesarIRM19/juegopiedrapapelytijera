package Interfaz;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTextField;

import Logica.Creditos;
import Logica.PPT;

public class Ventana extends JFrame implements ActionListener{

	private PanelUyM panelUsuario;
	private PanelUyM panelMaquina;
	private PanelCreditosPPT panelPiedra;
	private PanelCreditosPPT panelPapel;
	private PanelCreditosPPT panelTijera;
	private Creditos pi;
	private  int valorTexto = 100;
	JButton boton1, boton2, boton3;
	JTextField cuadro;
	
	public Ventana() {
		
		//setSize(1500,800);
	
		iniciarComponentes();
		this.pack();
	}

	private void iniciarComponentes() {
		colocarPaneles();
		colocarCreditos();
		colocarPaneles2();
		boton1.addActionListener(this);
		boton3.addActionListener(this);
		/*
		colocarEtiquetas();
		comboBox();
		colocarBoton();
		colocarSpinner();
		colocarCuadrodeTexto();
		*/
	
	
	
	}

	public void colocarPaneles() {
		this.panelUsuario = new PanelUyM();
		this.panelMaquina = new PanelUyM();
		JPanel panelDuelo;
		JLabel etiqueta2 = new JLabel(new ImageIcon("Media/versus2.gif"));
		panelDuelo = new JPanel(new GridLayout(1,3));
		
		panelDuelo.add(panelUsuario);
		panelDuelo.add(etiqueta2);
		panelDuelo.add(panelMaquina);
		this.getContentPane().add(panelDuelo,BorderLayout.NORTH);
		
	}
	public void colocarCreditos() {
		this.panelPiedra = new PanelCreditosPPT();
		this.panelPapel = new PanelCreditosPPT();
		this.panelTijera = new PanelCreditosPPT();
		
		JPanel panelCreditos;
		JLabel etiqueta5 = new JLabel(new ImageIcon("Media/piedra.jpg"));

		JLabel etiqueta6 = new JLabel(new ImageIcon("Media/PAPEL.jpg"));

		JLabel etiqueta7 = new JLabel(new ImageIcon("Media/tijera.jpg"));
		
		panelCreditos = new JPanel(new GridLayout(3,2));
		
		panelCreditos.add(etiqueta5);
		panelCreditos.add(panelPiedra);
		panelCreditos.add(etiqueta6);
		panelCreditos.add(panelPapel);
		panelCreditos.add(etiqueta7);
		panelCreditos.add(panelTijera);

		this.getContentPane().add(panelCreditos,BorderLayout.WEST);
		
	}

	private void colocarPaneles2() {
		JPanel panel, panel2, panel3;
		
		cuadro = new JTextField(Integer.toString(valorTexto));
		
		JLabel etiqueta8 = new JLabel("Creditos:");
		JLabel etiqueta9 = new JLabel("                              ");
		boton1 = new JButton("Jugar");
		boton2 = new JButton("Editar");
		boton3 = new JButton("Listo");
		panel = new JPanel();
		panel2 = new JPanel();
		panel3 = new JPanel();
		
		panel.setLayout(new GridLayout(3,1));
		panel2.setLayout(new FlowLayout());
		panel3.setLayout(new GridLayout(2,1));
		panel.add(boton1);
		panel3.add(etiqueta8);
		panel3.add(cuadro);
		panel.add(panel3);
		panel2.add(boton2);
		panel2.add(boton3);
		panel.add(panel2);
		panel.setBackground(Color.WHITE);
		
		

		this.getContentPane().add(panel, BorderLayout.CENTER);
		this.getContentPane().add(etiqueta9, BorderLayout.EAST);	
	}
/*
	private void colocarEtiquetas() {

	JLabel etiqueta = new JLabel("Piedra papel o tijera");
	etiqueta.setBounds(70, 1, 200, 100);
	etiqueta.setForeground(Color.BLACK);
	etiqueta.setFont(new Font("arial",Font.ITALIC,20));
	panel.add(etiqueta);
	
	JLabel etiqueta2 = new JLabel(new ImageIcon("Media/versus2.gif"));
	etiqueta2.setBounds(550, 10, 700, 500);
	panel.add(etiqueta2);
	
	JLabel etiqueta3 = new JLabel(new ImageIcon("Media/ring.jpg"));
	etiqueta3.setBounds(100, 10, 1600, 500);
	panel.add(etiqueta3);
	
	JLabel etiqueta4 = new JLabel(new ImageIcon("Media/sinrencores.gif"));
	etiqueta4.setBounds(550, 500,700, 250);
	panel.add(etiqueta4);
	
	JLabel etiqueta5 = new JLabel(new ImageIcon("Media/piedra.jpg"));
	etiqueta5.setBounds(100, 200,75, 75);
	panel.add(etiqueta5);
	
	JLabel etiqueta6 = new JLabel(new ImageIcon("Media/PAPEL.jpg"));
	etiqueta6.setBounds(100, 300,75, 75);
	panel.add(etiqueta6);
	
	JLabel etiqueta7 = new JLabel(new ImageIcon("Media/tijera.jpg"));
	etiqueta7.setBounds(100, 400,75, 75);
	panel.add(etiqueta7);
	
	JLabel etiqueta8 = new JLabel("Creditos:");
	etiqueta8.setBounds(400, 60, 100, 40);
	panel.add(etiqueta8);
	
	

	}
	
	private void comboBox() {
		String[] juego = {"piedra","papel","tijera"};
		JComboBox Combobox = new JComboBox(juego);
		Combobox.setBounds(100, 100, 100, 40);
		panel.add(Combobox);	
	}
	
	private void colocarBoton() {
		JButton boton1 = new JButton("Hacele");
		boton1.setBounds(210, 100, 100, 40);
		panel.add(boton1);
		
		JButton boton2 = new JButton("Editar");
		boton2.setBounds(100, 550, 100, 40);
		panel.add(boton2);
		
		JButton boton3 = new JButton("Listo");
		boton3.setBounds(210, 550, 100, 40);
		panel.add(boton3);
		
	}
	
	private void colocarSpinner() {
		JSpinner spinner = new JSpinner();
		spinner.setBounds(250, 220, 100, 40);
		panel.add(spinner);
		
		JSpinner spinner2 = new JSpinner();
		spinner2.setBounds(250, 320, 100, 40);
		panel.add(spinner2);
		
		JSpinner spinner3 = new JSpinner();
		spinner3.setBounds(250, 420, 100, 40);
		panel.add(spinner3);
		
	}
	
	private void colocarCuadrodeTexto() {
		JTextField cuadro = new JTextField();
		panel.add(cuadro);
	}
		
		
		*/
	public int obtenertText() {
		
		return valorTexto;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == boton1) {
			PPT cadena = new PPT();
			PPT op = new PPT();
			this.pi = new Creditos();
	        String opera = this.panelUsuario.obtener();
	        int text;
	        int apu;
	        int x = op.maquina();
	        String cad = cadena.cadena(x);
	        
	        try
	        {
	        	switch(opera)
	            {
	                case "Piedra":
	                    apu = op.piedra(x,opera,cad);
	                    text =  this.pi.apuestaValor(apu);
	                    System.out.println(text);
	                    this.valorTexto = text;
	                    
	                break;

	                case "Papel":

	                	apu = op.piedra(x,opera,cad);
	                    text =  this.pi.apuestaValor(apu);
	                    System.out.println(text);
	                    this.valorTexto = text;

	                break;

	                case "Tijera":

	                	apu = op.piedra(x,opera,cad);
	                    text =  this.pi.apuestaValor(apu);
	                    System.out.println(text);
	                    this.valorTexto = text;

	                break;
	            }
	        }catch(Exception e2)
	        {
	            JOptionPane.showMessageDialog(this, e2.getMessage());
	        
	        }
		} if(e.getSource() == boton2) {
			
		}if(e.getSource() == boton3) {
			System.out.println("1");
		}
		
		/*PPT cadena = new PPT();
		PPT op = new PPT();
        String opera = this.panelUsuario.obtener();
        int pie = this.panelPiedra.obtener();
        int pap = this.panelPapel.obtener();
        int tri = this.panelTijera.obtener(); 
        int x = op.maquina();
        String cad = cadena.cadena(x);
        
        try
        {
        	switch(opera)
            {
                case "Piedra":
                    op.piedra(x,opera,cad);

                break;

                case "Papel":

                    op.papel(x,opera,cad);

                break;

                case "Tijera":

                    op.tijera(x,opera,cad);

                break;
            }
        }catch(Exception e2)
        {
            JOptionPane.showMessageDialog(this, e2.getMessage());
        
        }
	*/	
	}
		

		
}

