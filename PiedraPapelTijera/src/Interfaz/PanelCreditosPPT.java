package Interfaz;

import java.awt.FlowLayout;
import java.awt.GridLayout;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;

public class PanelCreditosPPT extends JPanel {
	private JSpinner apuesta;
	private Ventana v;
	
	public PanelCreditosPPT() {
		SpinnerNumberModel model1 = new SpinnerNumberModel(1,1,8,1);

		
		this.apuesta = new JSpinner(model1);

		this.apuesta.setBounds(0, 0, 50, getHeight());
		this.setLayout(new FlowLayout());
		
		this.add(apuesta);
		//apuesta.setEnabled(false);
	}
	public int obtener() {
		int n;
		n = (Integer) apuesta.getValue();
		return n;
	}
	
	
	/*public void bloquear() {
		apuesta.setEnabled(false); 
	}
	public void desbloquear() {
		apuesta.setEnabled(true); 
	}
	/*public void habilita() {
	   
	   val1=(Integer) spin1.getValue();
	   val2=(Integer)spin2.getValue();
	   
	   val3=(Integer)spin3.getValue();
	      
	   if((val1>=1 && val1<=8) && (val2>=1 && val2<=8) && (val3>=1 && val3<=8)) {
		  
		   //botonacep.setEnabled(true);   
	   }
	}
	*/
}
